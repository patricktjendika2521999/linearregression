﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace LinearRegression
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            chartLineR.Titles.Add("Linear Regression");
            chartLineR.ChartAreas.Clear();
            chartLineR.ChartAreas.Add("Default");
            chartLineR.ChartAreas["Default"].AxisX.MajorGrid.LineColor = Color.SkyBlue;
            chartLineR.ChartAreas["Default"].AxisY.MajorGrid.LineColor = Color.SkyBlue;
            chartLineR.Series.Clear();
            chartLineR.Series.Add("Value Point");
            chartLineR.Series["Value Point"].Color = Color.Red;
            chartLineR.Series["Value Point"].ChartType = SeriesChartType.Point;
            //chartLineR.Series.Add("Value Line");
            //chartLineR.Series["Value Line"].Color = Color.Yellow;
            //chartLineR.Series["Value Line"].ChartType = SeriesChartType.Line;
            chartLineR.Series.Add("Regresi Linier");
            chartLineR.Series["Regresi Linier"].Color = Color.Orange;
            chartLineR.Series["Regresi Linier"].ChartType = SeriesChartType.Line;

            chartMSE.Titles.Add("Cost Function");
            chartMSE.ChartAreas.Clear();
            chartMSE.ChartAreas.Add("Default");
            chartMSE.ChartAreas["Default"].AxisX.MajorGrid.LineColor = Color.SkyBlue;
            chartMSE.ChartAreas["Default"].AxisY.MajorGrid.LineColor = Color.SkyBlue;
            chartMSE.Series.Clear();
            chartMSE.Series.Add("Value Line");
            chartMSE.Series["Value Line"].Color = Color.Red;
            chartMSE.Series["Value Line"].ChartType = SeriesChartType.Line;

            btnPredict.Enabled = false;
        }
        int n;
        double alpha = 0.00001, theta0, theta1, costf;

        int j = 0;
        bool stop = false;
        int iterasi = 500;
        double temp0 = 1, temp1;

        private void buttonLearn_Click(object sender, EventArgs e)
        {
            stop = false;
            btnPredict.Enabled = false;
            btnLearn.Enabled = false;
            chartMSE.Series["Value Line"].Points.Clear();
            chartLineR.Series["Regresi Linier"].Points.Clear();
            j = 0;
            alpha = (Double)nmrAlpha.Value;
            theta0 = (Double)nmrTheta0.Value;
            theta1 = (Double)nmrTheta1.Value;
            iterasi = (Int32)nmrCount.Value;
            tmr.Enabled = true;
            lblPredict.Text = "-";
        }

        private void btnPredict_Click(object sender, EventArgs e)
        {
            double predict;
            predict = theta0 + theta1 * (Double)nmrBrain.Value;
            lblPredict.Text = predict.ToString();
        }

        private void tmr_Tick(object sender, EventArgs e)
        {
            if (!stop)
            {
                for (int i = 0; i < n; i++)
                {
                    h[i] = theta0 + theta1 * x[i];
                    t0[i] = h[i] - y[i];
                    t1[i] = t0[i] * x[i];
                    cf[i] = t0[i] * t0[i];
                }
                avgt0 = t0.Average();
                avgt1 = t1.Average();
                avgcf = cf.Average();
                theta0 = theta0 - alpha * avgt0;
                theta1 = theta1 - alpha * avgt1;

                chartLineR.Series["Regresi Linier"].Points.Clear();
                for (int i = 0; i < n; i++)
                {
                    chartLineR.Series["Regresi Linier"].Points.AddXY(x[i], h[i]);
                    chartMSE.Series["Value Line"].Points.AddXY(j + 1, avgcf);
                }
                //Console.WriteLine("j: " + j);
                //Console.WriteLine("iterasi: " + iterasi);
                lblMSE.Text = avgcf.ToString();
                lblTheta0.Text = theta0.ToString();
                lblTheta1.Text = theta1.ToString();
                if ((avgcf == costf) || (temp0 == theta0 && temp1 == theta1) || j + 1 == iterasi)
                {
                    stop = true;
                    tmr.Enabled = false;
                    btnPredict.Enabled = true;
                    btnLearn.Enabled = true;
                }

                temp0 = theta0;
                temp1 = theta1;
                costf = avgcf;
            }
            j++;
        }

        double[] x, y, h, t0, t1, cf;
        double avgt0, avgt1, avgcf;

        private void Form1_Load(object sender, EventArgs e)
        {
            //x = new double[5] { 1, 2, 3, 4, 5 };
            //y = new double[5] { 2, 5, 6, 8, 10 };
            x = new double[62] { 3.385, 0.480, 1.350, 465.000, 36.330, 27.660, 14.830, 1.040, 4.190, 0.425, 0.101, 0.920, 1.000, 0.005, 0.060, 3.500, 2.000, 1.700, 2547.000, 0.023, 187.100, 521.000, 0.785, 10.000, 3.300, 0.200, 1.410, 529.000, 207.000, 85.000, 0.750, 62.000, 6654.000, 3.500, 6.800, 35.000, 4.050, 0.120, 0.023, 0.010, 1.400, 250.000, 2.500, 55.500, 100.000, 52.160, 10.550, 0.550, 60.000, 3.600, 4.288, 0.280, 0.075, 0.122, 0.048, 192.000, 3.000, 160.000, 0.900, 1.620, 0.104, 4.235 };
            y = new double[62] { 44.500, 15.500, 8.100, 423.000, 119.500, 115.000, 98.200, 5.500, 58.000, 6.400, 4.000, 5.700, 6.600, 0.140, 1.000, 10.800, 12.300, 6.300, 4603.000, 0.300, 419.000, 655.000, 3.500, 115.000, 25.600, 5.000, 17.500, 680.000, 406.000, 325.000, 12.300, 1320.000, 5712.000, 3.900, 179.000, 56.000, 17.000, 1.000, 0.400, 0.250, 12.500, 490.000, 12.100, 175.000, 157.000, 440.000, 179.500, 2.400, 81.000, 21.000, 39.200, 1.900, 1.200, 3.000, 0.330, 180.000, 25.000, 169.000, 2.600, 11.400, 2.500, 50.400 };

            n = x.Length;

            h = new double[n];
            t0 = new double[n];
            t1 = new double[n];
            cf = new double[n];

            for (int i = 0; i < n; i++)
            {
                chartLineR.Series["Value Point"].Points.AddXY(x[i], y[i]);
                //chartLineR.Series["Value Line"].Points.AddXY(x[i], y[i]);
            }
        }
    }
}
