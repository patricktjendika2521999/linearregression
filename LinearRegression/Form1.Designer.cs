﻿namespace LinearRegression
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chartLineR = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartMSE = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.nmrCount = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblPredict = new System.Windows.Forms.Label();
            this.lblMSE = new System.Windows.Forms.Label();
            this.btnPredict = new System.Windows.Forms.Button();
            this.nmrBrain = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.nmrTheta0 = new System.Windows.Forms.NumericUpDown();
            this.nmrTheta1 = new System.Windows.Forms.NumericUpDown();
            this.nmrAlpha = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnLearn = new System.Windows.Forms.Button();
            this.tmr = new System.Windows.Forms.Timer(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblTheta0 = new System.Windows.Forms.Label();
            this.lblTheta1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.chartLineR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartMSE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmrCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmrBrain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmrTheta0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmrTheta1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmrAlpha)).BeginInit();
            this.SuspendLayout();
            // 
            // chartLineR
            // 
            chartArea3.Name = "ChartArea1";
            this.chartLineR.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.chartLineR.Legends.Add(legend3);
            this.chartLineR.Location = new System.Drawing.Point(12, 256);
            this.chartLineR.Name = "chartLineR";
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "Series1";
            this.chartLineR.Series.Add(series3);
            this.chartLineR.Size = new System.Drawing.Size(500, 425);
            this.chartLineR.TabIndex = 0;
            // 
            // chartMSE
            // 
            chartArea4.Name = "ChartArea1";
            this.chartMSE.ChartAreas.Add(chartArea4);
            legend4.Name = "Legend1";
            this.chartMSE.Legends.Add(legend4);
            this.chartMSE.Location = new System.Drawing.Point(545, 256);
            this.chartMSE.Name = "chartMSE";
            series4.ChartArea = "ChartArea1";
            series4.Legend = "Legend1";
            series4.Name = "Series1";
            this.chartMSE.Series.Add(series4);
            this.chartMSE.Size = new System.Drawing.Size(500, 425);
            this.chartMSE.TabIndex = 1;
            // 
            // nmrCount
            // 
            this.nmrCount.Location = new System.Drawing.Point(122, 105);
            this.nmrCount.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nmrCount.Name = "nmrCount";
            this.nmrCount.Size = new System.Drawing.Size(120, 22);
            this.nmrCount.TabIndex = 49;
            this.nmrCount.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(70, 107);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 17);
            this.label7.TabIndex = 48;
            this.label7.Text = "Iterasi";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 193);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 17);
            this.label6.TabIndex = 47;
            this.label6.Text = "Body Weight:";
            // 
            // lblPredict
            // 
            this.lblPredict.AutoSize = true;
            this.lblPredict.Location = new System.Drawing.Point(122, 193);
            this.lblPredict.Name = "lblPredict";
            this.lblPredict.Size = new System.Drawing.Size(13, 17);
            this.lblPredict.TabIndex = 46;
            this.lblPredict.Text = "-";
            // 
            // lblMSE
            // 
            this.lblMSE.AutoSize = true;
            this.lblMSE.Location = new System.Drawing.Point(458, 24);
            this.lblMSE.Name = "lblMSE";
            this.lblMSE.Size = new System.Drawing.Size(13, 17);
            this.lblMSE.TabIndex = 45;
            this.lblMSE.Text = "-";
            // 
            // btnPredict
            // 
            this.btnPredict.Location = new System.Drawing.Point(248, 156);
            this.btnPredict.Name = "btnPredict";
            this.btnPredict.Size = new System.Drawing.Size(75, 23);
            this.btnPredict.TabIndex = 44;
            this.btnPredict.Text = "Predict";
            this.btnPredict.UseVisualStyleBackColor = true;
            this.btnPredict.Click += new System.EventHandler(this.btnPredict_Click);
            // 
            // nmrBrain
            // 
            this.nmrBrain.DecimalPlaces = 3;
            this.nmrBrain.Location = new System.Drawing.Point(122, 157);
            this.nmrBrain.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nmrBrain.Name = "nmrBrain";
            this.nmrBrain.Size = new System.Drawing.Size(120, 22);
            this.nmrBrain.TabIndex = 43;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 159);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 17);
            this.label5.TabIndex = 42;
            this.label5.Text = "Brain Weight";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(411, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 17);
            this.label4.TabIndex = 41;
            this.label4.Text = "MSE:";
            // 
            // nmrTheta0
            // 
            this.nmrTheta0.DecimalPlaces = 3;
            this.nmrTheta0.Location = new System.Drawing.Point(122, 50);
            this.nmrTheta0.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nmrTheta0.Name = "nmrTheta0";
            this.nmrTheta0.Size = new System.Drawing.Size(120, 22);
            this.nmrTheta0.TabIndex = 40;
            // 
            // nmrTheta1
            // 
            this.nmrTheta1.DecimalPlaces = 3;
            this.nmrTheta1.Location = new System.Drawing.Point(122, 78);
            this.nmrTheta1.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nmrTheta1.Name = "nmrTheta1";
            this.nmrTheta1.Size = new System.Drawing.Size(120, 22);
            this.nmrTheta1.TabIndex = 39;
            // 
            // nmrAlpha
            // 
            this.nmrAlpha.DecimalPlaces = 7;
            this.nmrAlpha.Location = new System.Drawing.Point(122, 22);
            this.nmrAlpha.Name = "nmrAlpha";
            this.nmrAlpha.Size = new System.Drawing.Size(120, 22);
            this.nmrAlpha.TabIndex = 38;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(59, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 17);
            this.label3.TabIndex = 37;
            this.label3.Text = "Theta 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(59, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 17);
            this.label2.TabIndex = 36;
            this.label2.Text = "Theta 0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(72, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 17);
            this.label1.TabIndex = 35;
            this.label1.Text = "Alpha";
            // 
            // btnLearn
            // 
            this.btnLearn.Location = new System.Drawing.Point(248, 104);
            this.btnLearn.Name = "btnLearn";
            this.btnLearn.Size = new System.Drawing.Size(75, 23);
            this.btnLearn.TabIndex = 34;
            this.btnLearn.Text = "Process";
            this.btnLearn.UseVisualStyleBackColor = true;
            this.btnLearn.Click += new System.EventHandler(this.buttonLearn_Click);
            // 
            // tmr
            // 
            this.tmr.Interval = 1;
            this.tmr.Tick += new System.EventHandler(this.tmr_Tick);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(355, 50);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 17);
            this.label8.TabIndex = 50;
            this.label8.Text = "Theta 0 Akhir:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(355, 78);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(97, 17);
            this.label9.TabIndex = 51;
            this.label9.Text = "Theta 1 Akhir:";
            // 
            // lblTheta0
            // 
            this.lblTheta0.AutoSize = true;
            this.lblTheta0.Location = new System.Drawing.Point(458, 50);
            this.lblTheta0.Name = "lblTheta0";
            this.lblTheta0.Size = new System.Drawing.Size(13, 17);
            this.lblTheta0.TabIndex = 52;
            this.lblTheta0.Text = "-";
            // 
            // lblTheta1
            // 
            this.lblTheta1.AutoSize = true;
            this.lblTheta1.Location = new System.Drawing.Point(458, 78);
            this.lblTheta1.Name = "lblTheta1";
            this.lblTheta1.Size = new System.Drawing.Size(13, 17);
            this.lblTheta1.TabIndex = 53;
            this.lblTheta1.Text = "-";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1057, 693);
            this.Controls.Add(this.lblTheta1);
            this.Controls.Add(this.lblTheta0);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.nmrCount);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblPredict);
            this.Controls.Add(this.lblMSE);
            this.Controls.Add(this.btnPredict);
            this.Controls.Add(this.nmrBrain);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.nmrTheta0);
            this.Controls.Add(this.nmrTheta1);
            this.Controls.Add(this.nmrAlpha);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnLearn);
            this.Controls.Add(this.chartMSE);
            this.Controls.Add(this.chartLineR);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chartLineR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartMSE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmrCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmrBrain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmrTheta0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmrTheta1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmrAlpha)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chartLineR;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartMSE;
        private System.Windows.Forms.NumericUpDown nmrCount;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblPredict;
        private System.Windows.Forms.Label lblMSE;
        private System.Windows.Forms.Button btnPredict;
        private System.Windows.Forms.NumericUpDown nmrBrain;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nmrTheta0;
        private System.Windows.Forms.NumericUpDown nmrTheta1;
        private System.Windows.Forms.NumericUpDown nmrAlpha;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnLearn;
        private System.Windows.Forms.Timer tmr;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblTheta0;
        private System.Windows.Forms.Label lblTheta1;
    }
}

